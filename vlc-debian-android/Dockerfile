FROM debian:bookworm-20240311-slim

MAINTAINER VideoLAN roots <roots@videolan.org>

ENV IMAGE_DATE=202306021100

ENV ANDROID_NDK="/sdk/android-ndk" \
    ANDROID_SDK="/sdk/android-sdk-linux"

# If someone wants to use VideoLAN docker images on a local machine and does
# not want to be disturbed by the videolan user, we should not take an uid/gid
# in the user range of main distributions, which means:
# - Debian based: <1000
# - RPM based: <500 (CentOS, RedHat, etc.)
ARG VIDEOLAN_CI_UID=499

ARG CORES=8

ENV RUST_HOME=/opt/rust
ENV RUSTUP_HOME=$RUST_HOME/rustup
ENV CARGO_HOME=$RUST_HOME/cargo

ENV PATH=/sdk/android-ndk/toolchains/llvm/prebuilt/linux-x86_64/bin/:/opt/tools/bin:/opt/tools/libexec:$CARGO_HOME/bin:$PATH

RUN groupadd --gid ${VIDEOLAN_CI_UID} videolan && \
    useradd --uid ${VIDEOLAN_CI_UID} --gid videolan --create-home --shell /bin/bash videolan && \
    echo "videolan:videolan" | chpasswd && \
    mkdir -p $RUST_HOME && chown videolan $RUST_HOME && \
    mkdir -p /usr/share/man/man1 && \
    apt-get update && \
    apt-get -y dist-upgrade && \
    apt-get install --no-install-suggests --no-install-recommends -y \
        openjdk-17-jdk-headless ca-certificates autoconf m4 automake ant autopoint bison \
        flex build-essential libtool libtool-bin patch pkg-config cmake meson \
        git yasm g++ gettext ninja-build \
        wget expect unzip python3 python3-venv python3-setuptools python3-mako \
        locales libltdl-dev curl nasm gperf \
    && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 && \
    echo "export ANDROID_NDK=${ANDROID_NDK}" >> /etc/profile.d/vlc_env.sh && \
    echo "export ANDROID_SDK=${ANDROID_SDK}" >> /etc/profile.d/vlc_env.sh && \
    mkdir /sdk && cd /sdk && \
    ANDROID_NDK_VERSION=26b && \
    ANDROID_NDK_SHA256=ad73c0370f0b0a87d1671ed2fd5a9ac9acfd1eb5c43a7fbfbd330f85d19dd632 && \
    wget -q https://dl.google.com/android/repository/android-ndk-r$ANDROID_NDK_VERSION-linux.zip && \
    echo $ANDROID_NDK_SHA256 android-ndk-r$ANDROID_NDK_VERSION-linux.zip | sha256sum -c && \
    unzip android-ndk-r$ANDROID_NDK_VERSION-linux.zip && \
    rm -f android-ndk-r$ANDROID_NDK_VERSION-linux.zip && \
    ln -s android-ndk-r$ANDROID_NDK_VERSION android-ndk && \
    mkdir android-sdk-linux && \
    cd android-sdk-linux && \
    mkdir "licenses" && \
    echo "24333f8a63b6825ea9c5514f83c2829b004d1fee" > "licenses/android-sdk-license" && \
    echo "d56f5187479451eabf01fb78af6dfcb131a6481e" >> "licenses/android-sdk-license" && \
    echo "24333f8a63b6825ea9c5514f83c2829b004d1fee" >> "licenses/android-sdk-license" && \
    SDK_TOOLS_FILENAME=commandlinetools-linux-9477386_latest.zip && \
    wget -q https://dl.google.com/android/repository/$SDK_TOOLS_FILENAME && \
    SDK_TOOLS_SHA256=bd1aa17c7ef10066949c88dc6c9c8d536be27f992a1f3b5a584f9bd2ba5646a0 && \
    echo $SDK_TOOLS_SHA256 $SDK_TOOLS_FILENAME | sha256sum -c && \
    unzip $SDK_TOOLS_FILENAME && \
    rm -f $SDK_TOOLS_FILENAME && \
    cd /sdk/android-sdk-linux && \
    cmdline-tools/bin/sdkmanager --sdk_root=/sdk/android-sdk-linux/ "build-tools;26.0.1" "platform-tools" "platforms;android-26" && \
    chown -R videolan /sdk && \
    mkdir /build && \
    cd /build && \
    VLC_TOOLS_HASH=0ef4d6151ce7a4443d88269fcb6f78bc0a442e98 && \
    git clone -b master https://code.videolan.org/videolan/vlc.git && cd vlc && git checkout ${VLC_TOOLS_HASH} && \
    cd contrib && mkdir build-contribs && cd build-contribs && \
    TARGET_TUPLE=aarch64-linux-android && \
    ANDROID_ABI=arm64-v8a ANDROID_API=21 ../bootstrap --host=$TARGET_TUPLE --prefix=/opt/tools/dummy --enable-ad-clauses && \
    make -j$CORES list && \
    make -j$CORES tools && \
    rm -rf /build

ENV LANG en_US.UTF-8
USER videolan

RUN RUST_TARGETS="aarch64-linux-android x86_64-linux-android i686-linux-android" && \
    mkdir $RUST_HOME/build && \
    cd $RUST_HOME/build && \
    RUST_VERSION=1.79.0 && \
    RUSTUP_VERSION=1.27.1 && \
    RUSTUP_SHA256=f5ba37f2ba68efec101198dca1585e6e7dd7640ca9c526441b729a79062d3b77 && \
    wget -q "https://github.com/rust-lang/rustup/archive/refs/tags/$RUSTUP_VERSION.tar.gz" -O rustup-$RUSTUP_VERSION.tar.gz && \
    echo $RUSTUP_SHA256 rustup-$RUSTUP_VERSION.tar.gz | sha256sum -c && \
    tar xzfo rustup-$RUSTUP_VERSION.tar.gz && \
    cd rustup-$RUSTUP_VERSION && \
    ./rustup-init.sh --default-toolchain $RUST_VERSION --profile minimal -y --target $RUST_TARGETS && \
    rm -rf $RUST_HOME/build

RUN git config --global user.name "VLC Android" && \
    git config --global user.email buildbot@videolan.org
