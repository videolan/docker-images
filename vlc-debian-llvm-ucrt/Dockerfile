FROM debian:bookworm-20250224-slim

LABEL org.opencontainers.image.authors="roots@videolan.org"

ENV IMAGE_DATE=202405241004

# The wine SDK path differs from version to version, starting from the one in buster, it's
# located in /usr/include/wine/wine/windows/ instead of
# /usr/include/wine/windows/
ENV WINE_SDK_PATH=/usr/include/wine/wine/windows

ENV RUST_HOME=/opt/rust
ENV RUSTUP_HOME=$RUST_HOME/rustup
ENV CARGO_HOME=$RUST_HOME/cargo

RUN apt-get update -qq && mkdir -p /usr/share/man/man1 && \
    apt-get -y dist-upgrade && \
    apt-get install -qqy --no-install-suggests --no-install-recommends \
        git wget bzip2 file unzip libtool-bin pkg-config build-essential \
        automake yasm gettext autopoint vim ninja-build ant libgl-dev libegl-dev \
        winbind flex bison zip dos2unix p7zip-full gperf nsis nasm cmake \
        python3 python3-venv python3-setuptools python3-mako locales meson help2man libltdl-dev \
        ca-certificates curl default-jdk-headless gnupg procps libcurl4-gnutls-dev \
    && \
    dpkg --add-architecture i386 && \
    wget -nc -O /etc/apt/keyrings/winehq.asc https://dl.winehq.org/wine-builds/winehq.key && \
    echo "deb [ signed-by=/etc/apt/keyrings/winehq.asc ] https://dl.winehq.org/wine-builds/debian/ bookworm main" > /etc/apt/sources.list.d/winehq.list && \
    apt-get update && apt-get -y install winehq-stable && \
    apt-get clean -y && \
    rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 && \
    update-alternatives --install /usr/bin/python python /usr/bin/python3.11 1

ENV LANG=en_US.UTF-8

RUN git config --global user.name "LLVM MinGW" && \
    git config --global user.email root@localhost

WORKDIR /build

ENV TOOLCHAIN_PREFIX=/opt/llvm-mingw

ARG CORES=4

COPY patches /patches

RUN cd /build/ && \
    BREAKPAD_VERSION=0.1.4 && \
    BREAKPAD_SHA256=020953390cce3a04ba859801bf863689a74e2aca0bd48ff256055f1c9af0e33a && \
    wget -q https://download.videolan.org/pub/contrib/breakpad/breakpad-$BREAKPAD_VERSION.tar.gz && \
    echo $BREAKPAD_SHA256 breakpad-$BREAKPAD_VERSION.tar.gz | sha256sum -c && \
    tar xzf breakpad-$BREAKPAD_VERSION.tar.gz && \
    cd breakpad-$BREAKPAD_VERSION && patch -p1 < /patches/0001-Fix-for-non-constant-SIGSTKSZ.patch && patch -p1 < /patches/0001-Fix-compilation-with-recent-gcc.patch && \
    autoreconf -vif && mkdir build && cd build && \
    ../configure --enable-tools --disable-processor --prefix=/opt/breakpad && \
    make -j$CORES && make install && \
    rm -rf /build/*

ENV PATH=/opt/tools/bin:/opt/tools/libexec:$CARGO_HOME/bin:$PATH

ARG TOOLCHAIN_ARCHS="i686 x86_64 armv7 aarch64"

ARG DEFAULT_CRT=ucrt

# Build everything that uses the llvm monorepo. We need to build the mingw runtime before the compiler-rt/libunwind/libcxxabi/libcxx runtimes.
COPY build-llvm.sh strip-llvm.sh install-wrappers.sh build-mingw-w64.sh build-mingw-w64-tools.sh build-compiler-rt.sh build-libcxx.sh build-mingw-w64-libraries.sh ./
COPY wrappers/*.sh wrappers/*.c wrappers/*.h ./wrappers/
RUN ./build-llvm.sh $TOOLCHAIN_PREFIX && \
    ./strip-llvm.sh $TOOLCHAIN_PREFIX && \
    ./install-wrappers.sh $TOOLCHAIN_PREFIX && \
    MINGW_W64_VERSION=819a6ec2ea87c19814b287e21d65e0dc7f05abba ./build-mingw-w64.sh $TOOLCHAIN_PREFIX --with-default-msvcrt=$DEFAULT_CRT --with-default-win32-winnt=0xA00 && \
    ./build-mingw-w64-tools.sh $TOOLCHAIN_PREFIX && \
    ./build-compiler-rt.sh $TOOLCHAIN_PREFIX && \
    ./build-libcxx.sh $TOOLCHAIN_PREFIX && \
    ./build-mingw-w64-libraries.sh $TOOLCHAIN_PREFIX && \
    ./build-compiler-rt.sh $TOOLCHAIN_PREFIX --build-sanitizers && \
    rm -rf /build/* && \
    rm -rf /root/.wine /tmp/.wine-*

# Build libssp
COPY build-libssp.sh ./
RUN ./build-libssp.sh $TOOLCHAIN_PREFIX && \
    rm -rf /build/*

# Prepare wine installation
COPY scripts/wait_process.sh /opt/wine/
RUN chmod +x /opt/wine/wait_process.sh && \
    wget -q https://raw.githubusercontent.com/Winetricks/winetricks/20230212/src/winetricks -O /opt/wine/winetricks && \
    WINETRICKS_SHA256=524c3cd602ef222da3dc644a0a741edd8bca6dfb72ba3c63998a76b82f9e77b2 && \
    echo $WINETRICKS_SHA256 /opt/wine/winetricks | sha256sum -c && \
    chmod +x /opt/wine/winetricks && \
    WINE_MONO_VERSION=4.5.6 && mkdir -p /usr/share/wine/mono && \
    wget -q "https://download.videolan.org/contrib/wine-mono/wine-mono-$WINE_MONO_VERSION.msi" -O /usr/share/wine/mono/wine-mono-$WINE_MONO_VERSION.msi && \
    WINE_MONO_SHA256=ac681f737f83742d786706529eb85f4bc8d6bdddd8dcdfa9e2e336b71973bc25 && \
    echo $WINE_MONO_SHA256 /usr/share/wine/mono/wine-mono-$WINE_MONO_VERSION.msi | sha256sum -c && \
    chmod +x /usr/share/wine/mono/wine-mono-$WINE_MONO_VERSION.msi

ENV PATH=$TOOLCHAIN_PREFIX/bin:$PATH

# Build VLC tools
RUN cd /build && \
    VLC_TOOLS_HASH=0ef4d6151ce7a4443d88269fcb6f78bc0a442e98 && \
    git clone -b master https://code.videolan.org/videolan/vlc.git && cd vlc && git checkout ${VLC_TOOLS_HASH} && \
    cd contrib && mkdir build-contribs && cd build-contribs && \
    TARGET_TUPLE=x86_64-w64-mingw32 && \
    ../bootstrap --host=$TARGET_TUPLE --prefix=/opt/tools/dummy --enable-ad-clauses && \
    make -j$CORES list && \
    make -j$CORES tools && \
    rm -rf /build/*

# If someone wants to use VideoLAN docker images on a local machine and does
# not want to be disturbed by the videolan user, we should not take an uid/gid
# in the user range of main distributions, which means:
# - Debian based: <1000
# - RPM based: <500 (CentOS, RedHat, etc.)
ARG VIDEOLAN_UID=499

RUN addgroup --quiet --gid ${VIDEOLAN_UID} videolan && \
    adduser --quiet --uid ${VIDEOLAN_UID} --ingroup videolan videolan && \
    echo "videolan:videolan" | chpasswd && \
    mkdir -p $RUST_HOME && chown videolan $RUST_HOME

USER videolan

RUN RUST_TARGETS="x86_64-pc-windows-gnullvm,aarch64-pc-windows-gnullvm,i686-pc-windows-gnullvm" && \
    mkdir $RUST_HOME/build && \
    cd $RUST_HOME/build && \
    RUST_VERSION=1.79.0 && \
    RUSTUP_VERSION=1.28.0 && \
    RUSTUP_SHA256=b5172faabef6778322c14f10e96261f0663f8896c7be62109083d57db89a052c && \
    wget -q "https://github.com/rust-lang/rustup/archive/refs/tags/$RUSTUP_VERSION.tar.gz" -O rustup-$RUSTUP_VERSION.tar.gz && \
    echo $RUSTUP_SHA256 rustup-$RUSTUP_VERSION.tar.gz | sha256sum -c && \
    tar xzfo rustup-$RUSTUP_VERSION.tar.gz && \
    cd rustup-$RUSTUP_VERSION && \
    ./rustup-init.sh --default-toolchain $RUST_VERSION --profile minimal -y --target $RUST_TARGETS && \
    rm -rf $RUST_HOME/build

RUN wine wineboot --init && \
    /opt/wine/wait_process.sh wineserver && \
    /opt/wine/winetricks --unattended dotnet48 dotnet_verifier && \
    rm -rf ~/.cache/winetricks && \
    rm -rf /tmp/.wine-*
